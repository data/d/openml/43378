# OpenML dataset: Intel-Stock-Prices-Historical-Data-(INTC)

https://www.openml.org/d/43378

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Intel Corporation designs, manufactures, and sells essential technologies for the cloud, smart, and connected devices worldwide. The company operates through DCG, IOTG, Mobileye, NSG, PSG, CCG, and All Other segments. It offers platform products, such as central processing units and chipsets, and system-on-chip and multichip packages; and non-platform or adjacent products comprising accelerators, boards and systems, connectivity products, and memory and storage products. The company also provides Internet of things products, including high-performance compute solutions for targeted verticals and embedded applications; and computer vision and machine learning-based sensing, data analysis, localization, mapping, and driving policy technology. It serves original equipment manufacturers, original design manufacturers, and cloud service providers. The company has collaborations with UC San Francisco's Center for Digital Health Innovation, Fortanix, and Microsoft Azure to establish a computing platform with privacy-preserving analytics to accelerate the development and validation of clinical algorithms; and Inventec Corporation. Intel Corporation was founded in 1968 and is headquartered in Santa Clara, California.
Content
Here is a simple code used to download data.
import yfinance as yf   
df = yf.download(tickers="INTC")   
df.to_csv('INTC.csv') 

Acknowledgements
This data collected with one line code using yahoo finance and yfinance library.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43378) of an [OpenML dataset](https://www.openml.org/d/43378). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43378/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43378/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43378/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

